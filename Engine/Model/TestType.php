<?php

/**
 * @author 2XD 
*/
 class TestType {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "test_type";

    const ATTR_ID = "id";
    const ATTR_NAME = "name";

    private $id = null;
    private $name = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET name
    */
    public function Name($newName = null){
    if($newName != null)
        $this->name = $newName;
    else
        return $this->name;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all TestType from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get TestType by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
    * Insert TestType to database
    */
    public function add() {
        $data = array( 
             self::ATTR_ID => $this->Id() 
             ,self::ATTR_NAME => $this->Name() 
        ); 
        $this->db->insert(self::TABLE_NAME, $data);
    }

    /**
    * Update TestType to database
    */
    public function update() {
        $data = array( 
            self::ATTR_NAME => $this->Name() 
        ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

    /**
    * Delete TestType from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

}