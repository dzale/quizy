<?php

/**
 * @author 2XD 
*/
class Answers {

    //****** Database attributes******//

   /**
   * @var Instance of Database class itself
   */
    private $db = null;
    const TABLE_NAME = "answers";

    const ATTR_ID = "id";
    const ATTR_QUESTION_ID = "question_id";
    const ATTR_TEXT = "text";
    const ATTR_CORRECT = "correct";
    const ATTR_TIMESTAMP = "timestamp";

    private $id = null;
    private $question_id = null;
    private $text = null;
    private $correct = null;
    private $timestamp = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET question_id
    */
    public function QuestionId($newQuestionId = null){
    if($newQuestionId != null)
        $this->question_id = $newQuestionId;
    else
        return $this->question_id;
    }

    /*
    * GET or SET text
    */
    public function Text($newText = null){
    if($newText != null)
        $this->text = $newText;
    else
        return $this->text;
    }

    /*
    * GET or SET correct
    */
    public function Correct($newCorrect = null){
    if($newCorrect != null)
        $this->correct = $newCorrect;
    else
        return $this->correct;
    }

    /*
    * GET or SET timestamp
    */
    public function Timestamp($newTimestamp = null){
    if($newTimestamp != null)
        $this->timestamp = $newTimestamp;
    else
        return $this->timestamp;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all Answers from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get Answers by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
     * Show Answers by QuestionId from database
     */
    public function showByQuestionId() {
        $query = "SELECT ".self::ATTR_TEXT.", ".self::ATTR_ID." FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_QUESTION_ID."` = :questionId;";
        return $this->db->select($query, array("questionId" => $this->QuestionId()));
    }
    
    /**
     * Get Answers by QuestionId from database
     */
    public function getByQuestionId() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_QUESTION_ID."` = :questionId;";
        return $this->db->select($query, array("questionId" => $this->QuestionId()));
    }

    /**
    * Insert Answers to database
    */
    public function add() {
        if($this->Id() == null ){
            $data = array( 
                 //self::ATTR_ID => $this->Id() 
                 self::ATTR_QUESTION_ID => $this->QuestionId() 
                 ,self::ATTR_TEXT => $this->Text() 
                 ,self::ATTR_CORRECT => $this->Correct() 
                 ,self::ATTR_TIMESTAMP => $this->Timestamp() 
            ); 
            $this->db->insert(self::TABLE_NAME, $data);
        }
        else{
            $this->update();
        }
    }

    /**
    * Update Answers to database
    */
    public function update() {
        if($this->Id() != null ){
            $data = array( 
                self::ATTR_QUESTION_ID => $this->QuestionId(), 
                self::ATTR_TEXT => $this->Text(), 
                self::ATTR_CORRECT => $this->Correct(), 
                self::ATTR_TIMESTAMP => $this->Timestamp() 
            ); 
            $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
        }
        else{
            $this->add();
        }
        
    }
    

    /**
    * Delete Answers from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }
    
    private function CreateFromDB($result){
        $this->SetAnswer($result);
    }
    
    public function SetAnswer($answer){
        
        if(isset($answer[self::ATTR_ID])){
            $this->ID($answer[self::ATTR_ID]);
        } 
        if(isset($answer[self::ATTR_TEXT])){
            $this->Text($answer[self::ATTR_TEXT]);
        }     
        if(isset($answer[self::ATTR_QUESTION_ID])){
            $this->QuestionId($answer[self::ATTR_QUESTION_ID]);
        }
        
        if(isset($answer[self::ATTR_CORRECT])){
            $this->Correct($answer[self::ATTR_CORRECT]);
        }
        
        $this->Timestamp(date('Y-m-d H:i:s'));       
    }

}