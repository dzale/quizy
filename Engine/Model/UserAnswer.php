<?php

/**
 * @author 2XD 
*/
 class UserAnswer   {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "user_answer";

    const ATTR_ID = "id";
    const ATTR_QUESTION_ID = "question_id";
    const ATTR_USER_ID = "user_id";
    const ATTR_ANSWER_ID = "answer_id";
    const ATTR_TIMESTAMP = "timestamp";

    private $id = null;
    private $question_id = null;
    private $user_id = null;
    private $answer_id = null;
    private $timestamp = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET question_id
    */
    public function QuestionId($newQuestionId = null){
    if($newQuestionId != null)
        $this->question_id = $newQuestionId;
    else
        return $this->question_id;
    }

    /*
    * GET or SET user_id
    */
    public function UserId($newUserId = null){
    if($newUserId != null)
        $this->user_id = $newUserId;
    else
        return $this->user_id;
    }

    /*
    * GET or SET answer_id
    */
    public function AnswerId($newAnswerId = null){
    if($newAnswerId != null)
        $this->answer_id = $newAnswerId;
    else
        return $this->answer_id;
    }

    /*
    * GET or SET timestamp
    */
    public function Timestamp($newTimestamp = null){
    if($newTimestamp != null)
        $this->timestamp = $newTimestamp;
    else
        return $this->timestamp;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all UserAnswer from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get UserAnswer by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
    * Insert UserAnswer to database
    */
    public function add() {
        $data = array( 
             self::ATTR_ID => $this->Id() 
             ,self::ATTR_QUESTION_ID => $this->QuestionId() 
             ,self::ATTR_USER_ID => $this->UserId() 
             ,self::ATTR_ANSWER_ID => $this->AnswerId() 
             ,self::ATTR_TIMESTAMP => $this->Timestamp() 
        ); 
        $this->db->insert(self::TABLE_NAME, $data);
    }

    /**
    * Update UserAnswer to database
    */
    public function update() {
        $data = array( 
            self::ATTR_QUESTION_ID => $this->QuestionId(), 
            self::ATTR_USER_ID => $this->UserId(), 
            self::ATTR_ANSWER_ID => $this->AnswerId(), 
            self::ATTR_TIMESTAMP => $this->Timestamp() 
        ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

    /**
    * Delete UserAnswer from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

}