<?php

/**
 * @author 2XD 
*/
 class Test {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "test";

    const ATTR_ID = "id";
    const ATTR_NAME = "name";
    const ATTR_PICTURE = "picture";
    const ATTR_SHUFLE = "shufle";
    const ATTR_SHARE_TEXT = "share_text";
    const ATTR_NUMBER_OF_QUESTION = "number_of_question";
    const ATTR_CREATED_AT = "created_at";

    private $id = null;
    private $name = null;
    private $picture = null;
    private $shufle = null;
    private $share_text = null;
    private $number_of_question = null;
    private $created_at = null;
    
    private $questions = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET name
    */
    public function Name($newName = null){
    if($newName != null)
        $this->name = $newName;
    else
        return $this->name;
    }

    /*
    * GET or SET picture
    */
    public function Picture($newPicture = null){
    if($newPicture != null)
        $this->picture = $newPicture;
    else
        return $this->picture;
    }

    /*
    * GET or SET shufle
    */
    public function Shufle($newShufle = null){
    if($newShufle != null)
        $this->shufle = $newShufle;
    else
        return $this->shufle;
    }

    /*
    * GET or SET share_text
    */
    public function ShareText($newShareText = null){
    if($newShareText != null)
        $this->share_text = $newShareText;
    else
        return $this->share_text;
    }

    /*
    * GET or SET number_of_question
    */
    public function NumberOfQuestion($newNumberOfQuestion = null){
    if($newNumberOfQuestion != null)
        $this->number_of_question = $newNumberOfQuestion;
    else
        return $this->number_of_question;
    }

    /*
    * GET or SET created_at
    */
    public function CreatedAt($newCreatedAt = null){
    if($newCreatedAt != null)
        $this->created_at = $newCreatedAt;
    else
        return $this->created_at;
    }

    
    /*
    * GET or SET questions
    */
    public function Questions($newQuestions = null){
    if($newQuestions != null)
        $this->questions = $newQuestions;
    else
        return $this->questions;
    }



    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all Test from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get Test by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
    * Get Test by  array of PrimaryKeys from database
    */
    public function getByPrimaryKeys($keys) {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` in (:primaryKeys);";
        return $this->db->select($query, array("primaryKeys" => join(", ", $keys)));
    }

     /** if ($show == true) => Function that loads all questions that are connected with this test 
      * if ($show == false) => Function that loads all questions and answers included correct answer, that are connected with this test
      * @return array
      */
    public function getQuestions($show = true){
        $question = new Question();
        
        $question->TestId($this->Id());

        $questions = $question->showByTestId();

        $answer = new Answers();

        $i = 0;
        foreach($questions as $question){

            $answer->QuestionId($question[Question::ATTR_ID]);

            if($show){
                $questions[$i++]["answers"] = $answer->showByQuestionId();
            }
            else{
                $questions[$i++]["answers"] = $answer->getByQuestionId();
            }

         }

        return $questions;
    }

    /**
    * Get Test by id with all questions and answers 
    */
    public function getTest() {
        $test = $this->getByPrimaryKey();
        
        $test[0]["questions"] = $this->getQuestions(false);
        
        $test[0]["userInfo"] = $this->userInfo();
        
        return $test[0];
    }
    
    /**
    * Get user info by test ID
    */
    public function userInfo() {
        $u = new UserDBConfig();
        $u->TestId($this->Id());
        $userInfo = $u->getByTestID();
        return $userInfo[0];
    }
    /**
    * Insert Test to database
    */
    public function add() {
        $photoManager = new PhotoManager($this->Picture());

        $data = array( 
//            self::ATTR_ID => $this->Id() 
            self::ATTR_NAME => $this->Name() 
            ,self::ATTR_PICTURE => $photoManager->GetPhotoName()
            ,self::ATTR_SHUFLE => $this->Shufle() 
            ,self::ATTR_SHARE_TEXT => $this->ShareText() 
            ,self::ATTR_NUMBER_OF_QUESTION => $this->NumberOfQuestion() 
            ,self::ATTR_CREATED_AT => $this->CreatedAt() 
        ); 
        $this->db->insert(self::TABLE_NAME, $data);
        $this->Id($this->db->lastInsertId());

        $photoManager->uploadAndResizePhoto(dirname(__FILE__) . "/../../upload/tests");

       foreach ($this->Questions() as $question) {
           $question[Question::ATTR_TEST_ID] = $this->Id();
           $q = new Question();
           $q->SetQuestion($question);
           $q->add();
       }
    }

    /**
    * Update Test to database
    */
    public function update() {
        $photoManager = new PhotoManager($this->Picture());
        $data = array( 
            self::ATTR_NAME => $this->Name(), 
            self::ATTR_PICTURE => $photoManager->GetPhotoName(), 
            self::ATTR_SHUFLE => $this->Shufle(), 
            self::ATTR_SHARE_TEXT => $this->ShareText(), 
            self::ATTR_NUMBER_OF_QUESTION => $this->NumberOfQuestion() 
            //self::ATTR_CREATED_AT => $this->CreatedAt() 
        ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    
        $photoManager->uploadAndResizePhoto(dirname(__FILE__) . "/../../upload/tests");

        $q = new Question();
        
        $q->TestId($this->Id());

        $oldQuestionsIds = array_column($q->showByTestId(), Question::ATTR_ID);
        
        foreach ($this->Questions() as $question) {
            
            $question[Question::ATTR_TEST_ID] = $this->Id();
            $q = new Question();
            $q->SetQuestion($question);
            if($q->Id() != null){
                if(($key = array_search($q->Id(), $oldQuestionsIds)) !== false) {
                    unset($oldQuestionsIds[$key]);
                }
                else{
                    $q->add();
                }
            }
            $q->update();
        }
        foreach ($oldQuestionsIds as $id) {
            $q = new Question();
            $q->Id($id);
            $q->delete();
        }
    }
    
    /**
    * Delete Test from database
    */
    public function delete() {
        
        $what = array(self::TABLE_NAME, Question::TABLE_NAME, Answers::TABLE_NAME);
        
        $leftJoin = " LEFT JOIN ";
        
        $on = " ON ";
        $from = self::TABLE_NAME;
        
        $from .= $leftJoin.Question::TABLE_NAME.$on."`".self::TABLE_NAME."`".".`".self::ATTR_ID."` = "."`".Question::TABLE_NAME."`".".`".Question::ATTR_TEST_ID."`";
        $from .= $leftJoin.Answers::TABLE_NAME.$on."`".Question::TABLE_NAME."`".".`".Question::ATTR_ID."` = "."`".Answers::TABLE_NAME."`".".`".Answers::ATTR_QUESTION_ID."`";
                
        $this->db->delete($from, "`".self::TABLE_NAME."`".".`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()), join(", ", $what));
    }
    
    private function CreateFromDB($result){
        $this->SetTest($result);
    }
    
    public function SetTest($test){
        
        if(isset($test[self::ATTR_ID])){
            $this->ID($test[self::ATTR_ID]);
        } 
        if(isset($test[self::ATTR_NAME])){
            $this->Name($test[self::ATTR_NAME]);
        }     
        if(isset($test[self::ATTR_NUMBER_OF_QUESTION])){
            $this->NumberOfQuestion($test[self::ATTR_NUMBER_OF_QUESTION]);
        }
        
        if(isset($test[self::ATTR_SHUFLE])){
            $this->Shufle($test[self::ATTR_SHUFLE]);
        }
        
        if(isset($test[self::ATTR_PICTURE])){
            $this->Picture($test[self::ATTR_PICTURE]);
        }
        
        if(isset($test[self::ATTR_SHARE_TEXT])){
            $this->ShareText($test[self::ATTR_SHARE_TEXT]);
        }
        
        $this->CreatedAt(date('Y-m-d H:i:s'));
        
        if(isset($test["questions"])){
            $this->Questions($test["questions"]);
        }
        
    }

}