<?php

/**
 * @author 2XD 
*/
 class User {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "user";

    const ATTR_ID = "id";
    const ATTR_NAME = "name";
    const ATTR_USERNAME = "username";
    const ATTR_OLD = "old";
    const ATTR_GENDER = "gender";
    const ATTR_IP_ADDRESS = "ip_address";
    const ATTR_CREATED_AT = "created_at";

    private $id = null;
    private $name = null;
    private $username = null;
    private $old = null;
    private $gender = null;
    private $ip_address = null;
    private $created_at = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET name
    */
    public function Name($newName = null){
    if($newName != null)
        $this->name = $newName;
    else
        return $this->name;
    }

    /*
    * GET or SET username
    */
    public function Username($newUsername = null){
    if($newUsername != null)
        $this->username = $newUsername;
    else
        return $this->username;
    }

    /*
    * GET or SET old
    */
    public function Old($newOld = null){
    if($newOld != null)
        $this->old = $newOld;
    else
        return $this->old;
    }

    /*
    * GET or SET gender
    */
    public function Gender($newGender = null){
    if($newGender != null)
        $this->gender = $newGender;
    else
        return $this->gender;
    }

    /*
    * GET or SET ip_address
    */
    public function IpAddress($newIpAddress = null){
    if($newIpAddress != null)
        $this->ip_address = $newIpAddress;
    else
        return $this->ip_address;
    }

    /*
    * GET or SET created_at
    */
    public function CreatedAt($newCreatedAt = null){
    if($newCreatedAt != null)
        $this->created_at = $newCreatedAt;
    else
        return $this->created_at;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all User from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get User by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
    * Insert User to database
    */
    public function add() {
        $data = array( 
             self::ATTR_ID => $this->Id() 
             ,self::ATTR_NAME => $this->Name() 
             ,self::ATTR_USERNAME => $this->Username() 
             ,self::ATTR_OLD => $this->Old() 
             ,self::ATTR_GENDER => $this->Gender() 
             ,self::ATTR_IP_ADDRESS => $this->IpAddress() 
             ,self::ATTR_CREATED_AT => $this->CreatedAt() 
        ); 
        $this->db->insert(self::TABLE_NAME, $data);
    }

    /**
    * Update User to database
    */
    public function update() {
        $data = array( 
            self::ATTR_NAME => $this->Name(), 
            self::ATTR_USERNAME => $this->Username(), 
            self::ATTR_OLD => $this->Old(), 
            self::ATTR_GENDER => $this->Gender(), 
            self::ATTR_IP_ADDRESS => $this->IpAddress(), 
            self::ATTR_CREATED_AT => $this->CreatedAt() 
        ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

    /**
    * Delete User from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }
    
    private function CreateFromDB($result){
        $this->SetUser($result);
    }
    
    public function SetUser($user){
        
        if(isset($user[self::ATTR_ID])){
            $this->ID($user[self::ATTR_ID]);
        } 
        if(isset($user[self::ATTR_NAME])){
            $this->Name($user[self::ATTR_NAME]);
        }     
        if(isset($user[self::ATTR_USERNAME])){
            $this->Username($user[self::ATTR_USERNAME]);
        }
        
        if(isset($user[self::ATTR_GENDER])){
            $this->Gender($user[self::ATTR_GENDER]);
        }
        
        if(isset($user[self::ATTR_IP_ADDRESS])){
            $this->IpAddress($user[self::ATTR_IP_ADDRESS]);
        }
        
        if(isset($user[self::ATTR_OLD])){
            $this->Old($user[self::ATTR_OLD]);
        }
        
        $this->CreatedAt(date('Y-m-d H:i:s'));
            
    }

}