<?php

/**
 * @author 2XD 
*/
 class TestUser {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "test_user";

    const ATTR_ID = "id";
    const ATTR_USER_ID = "user_id";
    const ATTR_TEST_ID = "test_id";
    const ATTR_TIMESTAMP = "timestamp";

    private $id = null;
    private $user_id = null;
    private $test_id = null;
    private $timestamp = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET user_id
    */
    public function UserId($newUserId = null){
    if($newUserId != null)
        $this->user_id = $newUserId;
    else
        return $this->user_id;
    }

    /*
    * GET or SET test_id
    */
    public function TestId($newTestId = null){
    if($newTestId != null)
        $this->test_id = $newTestId;
    else
        return $this->test_id;
    }

    /*
    * GET or SET timestamp
    */
    public function Timestamp($newTimestamp = null){
    if($newTimestamp != null)
        $this->timestamp = $newTimestamp;
    else
        return $this->timestamp;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all TestUser from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get TestUser by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    /**
    * Insert TestUser to database
    */
    public function add() {
        $data = array( 
             self::ATTR_ID => $this->Id() 
             ,self::ATTR_USER_ID => $this->UserId() 
             ,self::ATTR_TEST_ID => $this->TestId() 
             ,self::ATTR_TIMESTAMP => $this->Timestamp() 
        ); 
        $this->db->insert(self::TABLE_NAME, $data);
    }

    /**
    * Update TestUser to database
    */
    public function update() {
        $data = array( 
            self::ATTR_USER_ID => $this->UserId(), 
            self::ATTR_TEST_ID => $this->TestId(), 
            self::ATTR_TIMESTAMP => $this->Timestamp() 
        ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

    /**
    * Delete TestUser from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

}