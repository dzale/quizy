<?php

/**
 * @author 2XD 
*/
class Question {

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "question";

    const ATTR_ID = "id";
    const ATTR_TEST_ID = "test_id";
    const ATTR_TEXT = "text";
    const ATTR_POINTS = "points";
    const ATTR_CREATED_AT = "created_at";

    private $id = null;
    private $test_id = null;
    private $text = null;
    private $points = null;
    private $created_at = null;
    private $answers = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }

    /*
    * GET or SET test_id
    */
    public function TestId($newTestId = null){
    if($newTestId != null)
        $this->test_id = $newTestId;
    else
        return $this->test_id;
    }

    /*
    * GET or SET text
    */
    public function Text($newText = null){
    if($newText != null)
        $this->text = $newText;
    else
        return $this->text;
    }

    /*
    * GET or SET points
    */
    public function Points($newPoints = null){
    if($newPoints != null)
        $this->points = $newPoints;
    else
        return $this->points;
    }

    /*
    * GET or SET created_at
    */
    public function CreatedAt($newCreatedAt = null){
    if($newCreatedAt != null)
        $this->created_at = $newCreatedAt;
    else
        return $this->created_at;
    }
    
    /*
    * GET or SET answers
    */
    public function Answers($newAnswers = null){
    if($newAnswers != null)
        $this->answers = $newAnswers;
    else
        return $this->answers;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all Question from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get Question by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->id));
    }

    public function showByTestId(){
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_TEST_ID."` = :testID;";
        return $this->db->select($query, array("testID" => $this->TestId()));
    }
    
    /**
    * Insert Question to database
    */
    public function add() {
        if($this->Id() == null ){
            $data = array( 
                //self::ATTR_ID => $this->Id() 
               self::ATTR_TEST_ID => $this->TestId() 
               ,self::ATTR_TEXT => $this->Text() 
               ,self::ATTR_POINTS => $this->Points() 
               ,self::ATTR_CREATED_AT => $this->CreatedAt() 
             ); 
            $this->db->insert(self::TABLE_NAME, $data);
            $this->Id($this->db->lastInsertId());

            foreach ($this->Answers() as $answer) {
                $answer[Answers::ATTR_QUESTION_ID] = $this->Id();
                $a = new Answers();
                $a->SetAnswer($answer);
                $a->add();
            }
        }
        else{
            $this->update();
        }
    }

    /**
    * Update Question to database
    */
    public function update() {
        if($this->Id() != null ){
            $data = array( 
                self::ATTR_TEST_ID => $this->TestId(), 
                self::ATTR_TEXT => $this->Text(), 
                self::ATTR_POINTS => $this->Points() 
                //self::ATTR_CREATED_AT => $this->CreatedAt() 
            ); 
            $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
            
            $a = new Answers();
        
            $a->QuestionId($this->Id());

            $oldAnswersIds = array_column($a->showByQuestionId(), Answers::ATTR_ID);

            foreach ($this->Answers() as $answer) {

                $answer[Answers::ATTR_QUESTION_ID] = $this->Id();
                $a = new Answers();
                $a->SetAnswer($answer);
                if($a->Id() != null){
                    if(($key = array_search($a->Id(), $oldAnswersIds)) !== false) {
                        unset($oldAnswersIds[$key]);
                    }
                    else{
                        $a->add();
                    }
                }
                $a->update();
            }
            foreach ($oldAnswersIds as $id) {
                $a = new Answers();
                $a->Id($id);
                $a->delete();
            }
        }
        else{
            $this->add();
        }
    }
    /**
    * Delete Question from database
    */
    public function delete() {
        //$this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
        
        $what = array(self::TABLE_NAME, Answers::TABLE_NAME);
        
        $leftJoin = " LEFT JOIN ";
        
        $on = " ON ";
        $from = self::TABLE_NAME;
        
        $from .= $leftJoin.Answers::TABLE_NAME.$on."`".self::TABLE_NAME."`".".`".self::ATTR_ID."` = "."`".Answers::TABLE_NAME."`".".`".Answers::ATTR_QUESTION_ID."`";
                
        $this->db->delete($from, "`".self::TABLE_NAME."`".".`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()), join(", ", $what));
    
    }
    
    private function CreateFromDB($result){
        $this->SetQuestion($result);
    }
    
    public function SetQuestion($question){
        
        if(isset($question[self::ATTR_ID])){
            $this->ID($question[self::ATTR_ID]);
        } 
        if(isset($question[self::ATTR_TEXT])){
            $this->Text($question[self::ATTR_TEXT]);
        }     
        if(isset($question[self::ATTR_TEST_ID])){
            $this->TestId($question[self::ATTR_TEST_ID]);
        }
        
        if(isset($question[self::ATTR_POINTS])){
            $this->Points($question[self::ATTR_POINTS]);
        }
        
        $this->CreatedAt(date('Y-m-d H:i:s'));
        
        if(isset($question["answers"])){
            $this->Answers($question["answers"]);
        }
        
    }
}