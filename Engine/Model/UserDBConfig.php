<?php

/**
 * @author 2XD 
*/
class UserDBConfig{

    //****** Database attributes******//

    /**
    * @var Instance of Database class itself
    */
    private $db = null;
    const TABLE_NAME = "user_db_config";

    const ATTR_ID = "id";
    const ATTR_TEST_ID = "test_id";
    const ATTR_DB_USERNAME_TABLE = "db_username_table";
    const ATTR_DB_USERNAME_FIELD = "db_username_field";
    const ATTR_DB_USERNAME_ID = "db_username_id";
    const ATTR_DB_NAME_TABLE = "db_name_table";
    const ATTR_DB_NAME_FIELD = "db_name_field";
    const ATTR_DB_NAME_ID = "db_name_id";
    const ATTR_DB_OLD_TABLE = "db_old_table";
    const ATTR_DB_OLD_FIELD = "db_old_field";
    const ATTR_DB_OLD_ID = "db_old_id";
    const ATTR_DB_GENDER_TABLE = "db_gender_table";
    const ATTR_DB_GENDER_FIELD = "db_gender_field";
    const ATTR_DB_GENDER_ID = "db_gender_id";
    const ATTR_CREATED_AT = "created_at";

    private $id = null;
    private $test_id = null;
    private $db_username_table = null;
    private $db_username_field = null;
    private $db_username_id = null;
    private $db_name_table = null;
    private $db_name_field = null;
    private $db_name_id = null;
    private $db_old_table = null;
    private $db_old_field = null;
    private $db_old_id = null;
    private $db_gender_table = null;
    private $db_gender_field = null;
    private $db_gender_id = null;
    private $created_at = null;

    /*
    * GET or SET id
    */
    public function Id($newId = null){
    if($newId != null)
        $this->id = $newId;
    else
        return $this->id;
    }
    /*
    * GET or SET test_id
    */
    public function TestId($newTestId = null){
    if($newTestId != null)
        $this->test_id = $newTestId;
    else
        return $this->test_id;
    }

    /*
    * GET or SET db_username_table
    */
    public function DBUsernameTable($newDBUsernameTable = null){
    if($newDBUsernameTable != null)
        $this->db_username_table = $newDBUsernameTable;
    else
        return $this->db_username_table;
    }

    /*
    * GET or SET db_username_field
    */
    public function DBUsernameField($newDBUsernameField = null){
    if($newDBUsernameField != null)
        $this->db_username_field = $newDBUsernameField;
    else
        return $this->db_username_field;
    }

    /*
    * GET or SET db_username_id
    */
    public function DBUsernameId($newDBUsernameId = null){
    if($newDBUsernameId != null)
        $this->db_username_id = $newDBUsernameId;
    else
        return $this->db_username_id;
    }

    /*
    * GET or SET db_name_table
    */
    public function DBNameTable($newDBNameTable = null){
    if($newDBNameTable != null)
        $this->db_name_table = $newDBNameTable;
    else
        return $this->db_name_table;
    }

    /*
    * GET or SET db_name_field
    */
    public function DBNameField($newDBNameField = null){
    if($newDBNameField != null)
        $this->db_name_field = $newDBNameField;
    else
        return $this->db_name_field;
    }

    /*
    * GET or SET db_name_id
    */
    public function DBNameId($newDBNameId = null){
    if($newDBNameId != null)
        $this->db_name_id = $newDBNameId;
    else
        return $this->db_name_id;
    }

    /*
    * GET or SET db_old_table
    */
    public function DBOldTable($newDBOldTable = null){
    if($newDBOldTable != null)
        $this->db_old_table = $newDBOldTable;
    else
        return $this->db_old_table;
    }

    /*
    * GET or SET db_old_field
    */
    public function DBOldField($newDBOldField = null){
    if($newDBOldField != null)
        $this->db_old_field = $newDBOldField;
    else
        return $this->db_old_field;
    }

    /*
    * GET or SET db_old_id
    */
    public function DBOldId($newDBOldId = null){
    if($newDBOldId != null)
        $this->db_old_id = $newDBOldId;
    else
        return $this->db_old_id;
    }

    /*
    * GET or SET db_gender_table
    */
    public function DBGenderTable($newDBGenderTable = null){
    if($newDBGenderTable != null)
        $this->db_gender_table = $newDBGenderTable;
    else
        return $this->db_gender_table;
    }

    /*
    * GET or SET db_gender_field
    */
    public function DBGenderField($newDBGenderField = null){
    if($newDBGenderField != null)
        $this->db_gender_field = $newDBGenderField;
    else
        return $this->db_gender_field;
    }

    /*
    * GET or SET db_gender_id
    */
    public function DBGenderId($newDBGenderId = null){
    if($newDBGenderId != null)
        $this->db_gender_id = $newDBGenderId;
    else
        return $this->db_gender_id;
    }

    /*
    * GET or SET created_at
    */
    public function CreatedAt($newCreatedAt = null){
    if($newCreatedAt != null)
        $this->created_at = $newCreatedAt;
    else
        return $this->created_at;
    }




    /**
    * Class constructor
    */
    public function __construct() {
        $this->db = Database::getInstance();
    }


    /**
    * Get all UserDBConfig from database
    */
    public function getAll() {
        $query = "SELECT * FROM `".self::TABLE_NAME."`";
        return $this->db->select($query);
    }

    /**
    * Get UserDBConfig by PrimaryKey from database
    */
    public function getByPrimaryKey() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_ID."` = :primaryKey;";
        return $this->db->select($query, array("primaryKey" => $this->Id()));
    }
    
    /**
    * Get UserDBConfig by TestID from database
    */
    public function getByTestID() {
        $query = "SELECT * FROM `".self::TABLE_NAME."` WHERE `".self::ATTR_TEST_ID."` = :testID;";
        return $this->db->select($query, array("testID" => $this->TestId()));
    }

    /**
    * Insert UserDBConfig to database
    */
    public function add() {
        $data = array( 
             //self::ATTR_ID => $this->Id() 
            self::ATTR_TEST_ID => $this->TestId() 
            ,self::ATTR_DB_USERNAME_TABLE => $this->DBUsernameTable() 
            ,self::ATTR_DB_USERNAME_FIELD => $this->DBUsernameField() 
            ,self::ATTR_DB_USERNAME_ID => $this->DBUsernameId() 
            ,self::ATTR_DB_NAME_TABLE => $this->DBNameTable() 
            ,self::ATTR_DB_NAME_FIELD => $this->DBNameField() 
            ,self::ATTR_DB_NAME_ID => $this->DBNameId() 
            ,self::ATTR_DB_OLD_TABLE => $this->DBOldTable() 
            ,self::ATTR_DB_OLD_FIELD => $this->DBOldField() 
            ,self::ATTR_DB_OLD_ID => $this->DBOldId() 
            ,self::ATTR_DB_GENDER_TABLE => $this->DBGenderTable() 
            ,self::ATTR_DB_GENDER_FIELD => $this->DBGenderField() 
            ,self::ATTR_DB_GENDER_ID => $this->DBGenderId() 
            ,self::ATTR_CREATED_AT => $this->CreatedAt() 
            ); 
        $this->db->insert(self::TABLE_NAME, $data);
    }

    /**
    * Update UserDBConfig to database
    */
    public function update() {
        $data = array( 
            self::ATTR_TEST_ID => $this->TestId(), 
            self::ATTR_DB_USERNAME_TABLE => $this->DBUsernameTable(), 
            self::ATTR_DB_USERNAME_FIELD => $this->DBUsernameField(), 
            self::ATTR_DB_USERNAME_ID => $this->DBUsernameId(), 
            self::ATTR_DB_NAME_TABLE => $this->DBNameTable(), 
            self::ATTR_DB_NAME_FIELD => $this->DBNameField(), 
            self::ATTR_DB_NAME_ID => $this->DBNameId(), 
            self::ATTR_DB_OLD_TABLE => $this->DBOldTable(), 
            self::ATTR_DB_OLD_FIELD => $this->DBOldField(), 
            self::ATTR_DB_OLD_ID => $this->DBOldId(), 
            self::ATTR_DB_GENDER_TABLE => $this->DBGenderTable(), 
            self::ATTR_DB_GENDER_FIELD => $this->DBGenderField(), 
            self::ATTR_DB_GENDER_ID => $this->DBGenderId(), 
            self::ATTR_CREATED_AT => $this->CreatedAt() 
            ); 
        $this->db->update(self::TABLE_NAME, $data,"`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }

    /**
    * Delete UserDBConfig from database
    */
    public function delete() {
        $this->db->delete(self::TABLE_NAME, "`".self::ATTR_ID."` = :primaryKey", array( "primaryKey" => $this->Id()));
    }
    
        
    private function CreateFromDB($result){
        $this->SetUserDBConfig($result);
    }
    /**
    * Copy data to UserDBConfig object
    */
    public function SetUserDBConfig($userConfig){
        
        if(isset($userConfig[self::ATTR_ID])){
            $this->ID($userConfig[self::ATTR_ID]);
        } 
        //Username//
        if(isset($userConfig[self::ATTR_DB_USERNAME_FIELD])){
            $this->DBUsernameField($userConfig[self::ATTR_DB_USERNAME_FIELD]);
        }     
        if(isset($userConfig[self::ATTR_DB_USERNAME_ID])){
            $this->DBUsernameId($userConfig[self::ATTR_DB_USERNAME_ID]);
        }
        if(isset($userConfig[self::ATTR_DB_USERNAME_TABLE])){
            $this->DBUsernameTable($userConfig[self::ATTR_DB_USERNAME_TABLE]);
        }
        //Name//
        if(isset($userConfig[self::ATTR_DB_NAME_FIELD])){
            $this->DBNameField($userConfig[self::ATTR_DB_NAME_FIELD]);
        }     
        if(isset($userConfig[self::ATTR_DB_NAME_ID])){
            $this->DBNameId($userConfig[self::ATTR_DB_NAME_ID]);
        }
        if(isset($userConfig[self::ATTR_DB_NAME_TABLE])){
            $this->DBNameTable($userConfig[self::ATTR_DB_NAME_TABLE]);
        }
        //Gender//
        if(isset($userConfig[self::ATTR_DB_GENDER_FIELD])){
            $this->DBGenderField($userConfig[self::ATTR_DB_GENDER_FIELD]);
        }     
        if(isset($userConfig[self::ATTR_DB_GENDER_ID])){
            $this->DBGenderId($userConfig[self::ATTR_DB_GENDER_ID]);
        }      
        if(isset($userConfig[self::ATTR_DB_GENDER_TABLE])){
            $this->DBGenderTable($userConfig[self::ATTR_DB_GENDER_TABLE]);
        }
        //Old//
        if(isset($userConfig[self::ATTR_DB_OLD_FIELD])){
            $this->DBOldField($userConfig[self::ATTR_DB_OLD_FIELD]);
        }     
        if(isset($userConfig[self::ATTR_DB_OLD_ID])){
            $this->DBOldId($userConfig[self::ATTR_DB_OLD_ID]);
        }      
        if(isset($userConfig[self::ATTR_DB_OLD_TABLE])){
            $this->DBOldTable($userConfig[self::ATTR_DB_OLD_TABLE]);
        }
                  
        $this->CreatedAt(date('Y-m-d H:i:s'));
            
    }
    
    
    /**
    * Set default values for UserDBConfig object
    */
    public function SetDefaultUserDBConfig($userConfig){
   
        //Username//
        $this->DBUsernameField(USERNAME_ATTR);
        $this->DBUsernameId(USERNAME_RELATION);
        $this->DBUsernameTable(USERNAME_TABLE);

        //Name//
        $this->DBNameField(NAME_ATTR);
        $this->DBNameId(NAME_RELATION);
        $this->DBNameTable(NAME_TABLE);

        //Gender//
        $this->DBGenderField(GENDER_ATTR);
        $this->DBGenderId(GENDER_RELATION);
        $this->DBGenderTable(GENDER_TABLE);

        //Old//
        $this->DBOldField(OLD_ATTR);
        $this->DBOldId(OLD_RELATION);
        $this->DBOldTable(OLD_TABLE);
  
        $this->CreatedAt(date('Y-m-d H:i:s'));
            
    }

}