<?php

/**
 * @author 2XD 
*/
class Response
{

    /** 200 OK
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _200($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_200, ResponseMessage::_200, ResponseStatus::Success, $message, $data);
    }

    /** 201 Created
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _201($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_201, ResponseMessage::_201, ResponseStatus::Success, $message, $data);
    }

    /** 204 No Content
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _204($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_204, ResponseMessage::_204, ResponseStatus::Error, $message, $data);
    }

    /** 304 Not Modified
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _304($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_304, ResponseMessage::_304, ResponseStatus::Error, $message, $data);
    }

    /** 400 Bad Request
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _400($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_400, ResponseMessage::_400, ResponseStatus::Error, $message, $data);
    }

    /** 401 Unauthorized
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _401($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_401, ResponseMessage::_401, ResponseStatus::Error, $message, $data);
    }

    /** 403 Forbidden
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _403($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_403, ResponseMessage::_403, ResponseStatus::Error, $message, $data);
    }

    /** 404 Not Found
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _404($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_404, ResponseMessage::_404, ResponseStatus::Error, $message, $data);
    }

    /** 405 Method Not Allowed"
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _405($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_405, ResponseMessage::_405, ResponseStatus::Error, $message, $data);
    }
    
    /** 408 Request Timeout"
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _408($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_408, ResponseMessage::_408, ResponseStatus::Error, $message, $data);
    }

    /** 410 Gone
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _410($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_410, ResponseMessage::_410, ResponseStatus::Error, $message, $data);
    }

    /** 415 Unsupported Media Type
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _415($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_415, ResponseMessage::_415, ResponseStatus::Error, $message, $data);
    }

    /** 422 Unprocessable Entity
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _422($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_422, ResponseMessage::_422, ResponseStatus::Error, $message, $data);
    }

    /** 429 Too Many Requests
     * @param $message Custom message to be sent to Client
     * @param null $data Data to be sent to client
     * @return Returns JSON TYPE RESPONSE
     */
    public static function _429($message = null, $data = null){
        return self::GenerateResponse(ResponseCode::_429, ResponseMessage::_429, ResponseStatus::Error, $message, $data);
    }


    private static function GenerateResponse($code, $codeMessage, $status, $message = null, $data = null){

        if($message == null){
            $message = $codeMessage;
        }
        http_response_code($code);
        $response = array(
            'code' => $code,
            'code_message' => $codeMessage,
            'status'  => $status,
            'message' => array($message),
            'data'    => $data
        );

        return json_encode($response);

    }
}