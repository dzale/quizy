<?php

/**
 * @author 2XD 
*/

abstract class ResponseCode
{

    const _200   = "200";
    const _201   = "201";
    const _204   = "204";
    const _304   = "304";
    const _400   = "400";
    const _401   = "401";
    const _403   = "403";
    const _404   = "404";
    const _405   = "405";
    const _408   = "408";
    const _410   = "410";
    const _415   = "415";
    const _422   = "422";
    const _429   = "429";

}