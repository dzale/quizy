<?php


class PhotoManager
{
    const AVATAR_WIDTH = 200;

    const AVATAR_HEIGHT = 200;

    /**
     * @var Base64 encoded string that contains image
     */
    private $photo;

    /**
     * @var string Attr that contains photo name
     */
    private $photoName;

    public function __construct($photo)
    {
        $this->photo = $photo;

        $this->photoName = $this->generatePhotoName();
    }

    /**
     * @return string Returns Photo Name
     */
    public function GetPhotoName(){
        return $this->photoName;
    }

    /**
     * Upload and crop user photo to predefined width and height.
     *
     * @param $path
     * @param $old_photo
     * @return string Photo file name.
     */
    public function uploadAndResizePhoto($path, $old_photo = null)
    {
        $name = $this->uploadFile($path, $old_photo);

        return $name;
    }

    /**
     * Get uploaded file from HTTP request.
     *
     * @return array|null|\Symfony\Component\HttpFoundation\File\UploadedFile
     */
    private function getUploadedFileFromRequest()
    {
        $img = str_replace('data:image/png;base64,', '', $this->photo);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);

        return $data;
    }

    /**
     * Upload photo photo and
     * delete old photo if exists.
     *
     * @param $path where should image be uploaded
     * @param $old_photo old photo
     * @return array
     */
    private function uploadFile($path, $old_photo)
    {
        if($old_photo != null){
            $this->deletePhotoIfUploaded($path."/".$old_photo);
        }

        $name = $this->GetPhotoName();

        $uploadedImg = $this->getUploadedFileFromRequest();

        $imgPath = $path."/".$name;

//        $newImage = fopen($imgPath, "w") or die("Unable to open file!");

  //      fclose($newImage);

        file_put_contents($imgPath, $uploadedImg);

        return $name;
    }

    /**
     * @param String $old_photo path to old photo
     */
    public function deletePhotoIfUploaded($old_photo)
    {
        unlink($old_photo);
    }

    /**
     * Build random photo name.
     *
     * @return string
     */
    private function generatePhotoName()
    {
        return sprintf(
            "%s.%s",
            md5($this->str_random(10) . time()),
            "png"
        );
    }

    private function str_random($length){

        $randomString = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);

        return $randomString;
    }

    /**
     * Resize image to predefined width and height.
     *
     * @param File $photoImage
     * @return \Intervention\Image\Image
     */
    private function resizeImage($file, $w, $h) {
        list($width, $height) = getimagesize($file);
        $r = $width / $height;

        if ($w/$h > $r) {
            $newwidth = $h*$r;
            $newheight = $h;
        } else {
            $newheight = $w/$r;
            $newwidth = $w;
        }

        $src = imagecreatefromjpeg($file);
        $dst = imagecreatetruecolor($newwidth, $newheight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

        return $dst;
    }
}