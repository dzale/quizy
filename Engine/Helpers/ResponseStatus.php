<?php

/**
 * @author 2XD 
*/
abstract class ResponseStatus
{
    const Error   = "error";
    const Success = "success";
}