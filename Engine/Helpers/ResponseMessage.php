<?php

/**
 * @author 2XD 
*/
abstract class ResponseMessage
{

    const _200   = "OK";
    const _201   = "Created";
    const _204   = "No Content";
    const _304   = "Not Modified";
    const _400   = "Bad Request";
    const _401   = "Unauthorized";
    const _403   = "Forbidden";
    const _404   = "Not Found";
    const _405   = "Method Not Allowed";
    const _408   = "Request Timeout";
    const _410   = "Gone";
    const _415   = "Unsupported Media Type";
    const _422   = "Unprocessable Entity";
    const _429   = "Too Many Requests";

}