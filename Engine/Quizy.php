<?php
include_once 'Config.php';
include_once 'Session.php';
include_once 'Lang.php';
include_once 'Database.php';
//MODEL
include_once 'Model/Answers.php';
include_once 'Model/Question.php';
include_once 'Model/Test.php';
include_once 'Model/TestType.php';
include_once 'Model/TestUser.php';
include_once 'Model/User.php';
include_once 'Model/UserAnswer.php';
include_once 'Model/UserDBConfig.php';
//RESPONSE
include_once 'Helpers/PhotoManager.php';
include_once 'Helpers/QuizyTemplates.php';
include_once 'Helpers/ResponseCode.php';
include_once 'Helpers/ResponseStatus.php';
include_once 'Helpers/ResponseMessage.php';
include_once 'Helpers/Response.php';


$db = Database::getInstance();

Session::startSession();

if ( isset ( $_GET['lang'] ) )
    Lang::setLanguage($_GET['lang']);

/**
 * Redirect to provided url
 * @param $url
 */
function redirect($url)
{
    $url = rtrim(WEBSITE_DOMAIN, '/') . '/' . ltrim($url, '/');

    if ( ! headers_sent() )
    {    
        header('Location: '.$url, TRUE, 302);
        exit;
    }
    else
    {
        echo '<script type="text/javascript">';
        echo 'window.location.href="'.$url.'";';
        echo '</script>';
        echo '<noscript>';
        echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
        echo '</noscript>';
        exit;
    }
}

function generate($template, $quizyIds = null){
        
    $quizies = null;
    
    $quizy = new Test();
    
    if($quizyIds == null){
        
        $quizies = $quizy->getAll();
    }
    else if (is_array($quizyIds)){
        $quizies = $quizy->getByPrimaryKeys($quizyIds);
    }
    else{
        $quizy->Id($quizyIds);
        $quizies = $quizy->getByPrimaryKey();
    }
    
    return render($template, $quizies);
}

function render($template, $param){
   ob_start();
   $quizies = $param;
   include(dirname(__FILE__)."/../Templates/".$template);//How to pass $param to it? It needs that $row to render blog entry!
   $ret = ob_get_contents();
   ob_end_clean();
   return $ret;
}
