<?php
include_once 'Quizy.php';

//csrf protection
if(empty($_SERVER['HTTP_X_REQUESTED_WITH']) || strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') 
    die("Sorry bro!");

$url = parse_url( isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '');
if( !isset( $url['host']) || ($url['host'] != $_SERVER['SERVER_NAME']))
    die("Sorry bro!");

$action = $_POST['action'];

switch ($action) {
    case 'login':
        echo Response::_401();//isLogedIn($_POST["username"], $_POST["password"]);
    break;

    case 'addTest':
        onlyAdmin();
        $t = new Test();
        $t->SetTest($_POST['test']);
        $t->add();
        $u = new UserDBConfig();
        $a = $_POST['defaultUserInfo'];
        if(($_POST['defaultUserInfo'])){
            $u->SetUserDBConfig($_POST['userInfo']);
        }
        else{
            $u->SetDefaultUserDBConfig();
        }
        $u->TestId($t->Id());
        $u->add();
        echo Response::_200();
    break;

    case 'updateTest':
        onlyAdmin();
        $t = new Test();
        $t->SetTest($_POST['test']);
        $t->update();
        echo Response::_200();
    break;
    
    case 'deleteTest':
        onlyAdmin();
        $t = new Test();
        $t->SetTest($_POST['test']);        
        echo $t->delete();
    break;

    case 'getTest':
        onlyAdmin();
        $t = new Test();
        $t->SetTest($_POST['test']);        
        echo Response::_200(null, $t->getTest());
    break;

    case 'showTest':
        onlyAdmin();
        $t = new Test();
        $t->SetTest($_POST['test']);        
        echo $t->getByPrimaryKey();
    break;

    case 'showAllTests':
        onlyAdmin();
        $t = new Test();                
        echo Response::_200(null, $t->getAll());
    break;

    case 'showTests':
        onlyAdmin();
        $t = new Test();  
        $keys = $_POST['keys'];
        echo $t->getByPrimaryKeys($keys);        
    break;


    default:
    break;
}

function onlyAdmin() {
    
}


function isLogedIn($us, $pass) {
    $message ="";
    if(LOGIN_ATTEMPT < MAX_LOGIN_ATTEMPT){
        if($us == ADMIN_USERNAME && $pass == ADMIN_PASSWORD){
            $message = "success";
        }
        else{
            $message = "wrong combination";
        }
    }
    else{
        $message = "max attempt";
    }
    return $message;
}


        