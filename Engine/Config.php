<?php

//WEBSITE

define('WEBSITE_NAME', "Quizy");

define('WEBSITE_DOMAIN', "http://localhost/quizy");


//DATABASE CONFIGURATION

define('DB_HOST', "localhost"); 

define('DB_TYPE', "mysql"); 

define('DB_USER', "root"); 

define('DB_PASS', ""); 

define('DB_NAME', "quizy"); 

//ADMIN CONFIGURATION

define('ADMIN_USERNAME', "admin");   

define('ADMIN_PASSWORD', "admin123");

define('MAX_LOGIN_ATTEMPT', 5);

define('LOGIN_ATTEMPT', 0);


//SESSION CONFIGURATION

define('SESSION_SECURE', false);   

define('SESSION_HTTP_ONLY', true);

define('SESSION_REGENERATE_ID', false);   

define('SESSION_USE_ONLY_COOKIES', 1);


//LOGIN CONFIGURATION

define('LOGIN_FINGERPRINT', true); 

define('SUCCESS_LOGIN_REDIRECT', "index.php"); 

//USER INFO

//username must be unique
define('USERNAME_TABLE', "user");

define('USERNAME_ATTR', "username"); 

define('USERNAME_RELATION', "id"); 

//name
define('NAME_TABLE', "user");

define('NAME_ATTR', "name"); 

define('NAME_RELATION', "id"); 

//old
define('OLD_TABLE', "user");

define('OLD_ATTR', "old"); 

define('OLD_RELATION', "id"); 

//gender
define('GENDER_TABLE', "user");

define('GENDER_ATTR', "gender"); 

define('GENDER_RELATION', "id"); 


// TRANSLATION

define('DEFAULT_LANGUAGE', 'en'); 


