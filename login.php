<?php   
    include 'templates/header.php';
?>

<div class="modal-dialog" id="login">
        <div class="modal-content">
            <div class="modal-header">
                <h3><?php echo Lang::get('welcome_text'); ?></h3>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#login-tab" data-toggle="tab"><?php echo Lang::get('login'); ?></a></li>
                </ul>

                <div class="tab-content top15">
                    <div class="tab-pane active fade in" id="login-tab">                        
                            <fieldset>
                                <div class="row control-group form-group">
                                    <!-- Username -->
                                    <label class="control-label col-lg-2"><?php echo Lang::get('username'); ?></label>
                                    <div class="controls col-lg-10">
                                        <input type="text" placeholder="" v-model="username" class="input-sm form-control"> <br />
                                    </div>
                                </div>

                                <div class="row control-group form-group">
                                    <!-- Password-->
                                    <label class="control-label col-lg-2"><?php echo Lang::get('password'); ?></label>
                                    <div class="controls col-lg-10">
                                        <input type="password" placeholder="" v-model="password" class="input-sm form-control">
                                    </div>
                                </div>


                                <div class="row control-group form-group">
                                    <!-- Button -->
                                    <div class="controls col-lg-offset-2 col-lg-8">
                                        <button id="btn-login" class="btn btn-success" v-on:click="checkLogin" v-bind:class="{'active': isFormSubmitted}">
                                            <span class="spinner">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </span>
                                                <?php echo Lang::get('login'); ?>
                                        </button>
                                    </div>
                                </div>
                            </fieldset>                        
                    </div>

                </div>
                <div class="row text-center">   
                    <p class="text-danger" v-for="error in errors">
                        {{ error }}
                    </p>
                </div> 
            </div>
        </div>
    <div>
    <script src="vendor/js/1_jquery.min.js" type="text/javascript"></script>
    <script src="vendor/js/2_jquery-ui.min.js" type="text/javascript"></script>
    <script src="vendor/js/3_bootstrap.min.js" type="text/javascript"></script>
    <script src="vendor/js/6_vue.js" type="text/javascript"></script>
    <script src="vendor/js/7_vue-resource.min.js" type="text/javascript"></script>
    <script src="./Library/js/Controllers/login.js" type="text/javascript"></script>