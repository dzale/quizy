//Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('value');

var uploadPhotoComponent = Vue.component('upload-photo-component', {
    props:['title', 'path', 'photoName'],
    template: '#upload-photo-component',
    data :function(){
        return {
            newPhotoSelected: false,
            croppieContainer: null
        };
    },
    ready: function() {

        this.initializeCroppie('square');

    },
    methods: {

        initializeCroppie: function(croppieType){


            this.croppieContainer = $('#croppie-container').croppie({
                viewport: {
                    width:  650,
                    height: 350,
                    type: croppieType
                },
                boundary: {
                    width: 975,
                    height: 525
                }
            });

        },

        onPhotoChanged: function(){
            this.readFile(document.getElementById("photo-upload"), this.croppieContainer);
            this.newPhotoSelected = true;
        },       
        
        onPhotoCropped: function(){
            this.newPhotoSelected = false;
            
            var res = this.croppieContainer.croppie('result', {
                type: 'canvas',
                size: 'viewport'
            });
            
            var component = this;
            
            res.then(function (resp) {
                
                component.$dispatch('photo-changed', resp);

                this.photoName = resp;

                document.getElementById('test-img').setAttribute( 'src', this.photoName);

            });
                        
        },

        readFile: function(input, croppieContainer) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    croppieContainer.croppie('bind', {
                        url: e.target.result
                    });
                    $('.upload-demo').addClass('ready');
                };

                reader.readAsDataURL(input.files[0]);
            }
            else {
                console.log("Sorry - you're browser doesn't support the FileReader API");
            }
}
    }

});