Vue.config.debug = true;
Vue.http.options.emulateJSON = true;

new Vue({
    el: '#all-tests',
    data : {
        isFormSubmitted : false,
        tests           : [],
        errors          : []
    },
    ready:function(){
        this.getTests();
    },

    methods: {

        getTests : function (){
            var data = {
                action: "showAllTests"
            };
            this.errors = [];
            this.$http.post('Engine/Routes.php',
                data,
                function(response, status, request){
                    
                    if(status == 200){                        
                        this.tests = response.data;
                    }
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        },
        
        editTest : function(test){
            console.log(test.id);
            window.location = "./newTest.php?id="+test.id;
        },
        
        deleteTest : function(test){
            var data = {
                action: "deleteTest",
                test:{
                  id : test.id  
                } 
            };
            this.errors = [];
            this.$http.post('Engine/Routes.php',
                data,
                function(response, status, request){
                    
                    if(status == 200){
                        this.tests.$remove(test);
                    }
                    
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        }
        
    }

      ,
    events: {
    }
  
});

