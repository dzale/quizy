/**
 * Created by ndzak on 2/17/2016.
 */

$(document).ready(function(){


    $(".quizy-answer").click(function(){

        var selectedColorClass = "btn-info";

        var defaultColorClass = "btn-white";

        var questionId = SingleTestController.GetActiveQuestionHTMLId();

        var selectedAnswerId = SingleTestController.GetSelectedAnswer();

        if(selectedAnswerId !== null){

            $(questionId + " " + selectedAnswerId).removeClass(selectedColorClass);

            $(questionId + " " + selectedAnswerId).addClass(defaultColorClass);

        }

        $(this).removeClass(defaultColorClass);

        $(this).addClass(selectedColorClass);

        SingleTestController.SetSelectedAnswer(this);

    });
});

var SingleTestController = {};

SingleTestController.activeQuestion = {
    id: 0,
    selectedAnswerId: -1
};

SingleTestController.GetActiveQuestionHTMLId = function(){
  return "#quizy-question-" + SingleTestController.activeQuestion.id;
};

SingleTestController.SetSelectedAnswer = function(elem){
    SingleTestController.activeQuestion.selectedAnswerId = $(elem).data("quizy-answer-id");
};

SingleTestController.GetSelectedAnswer = function(){
    if(SingleTestController.activeQuestion.selectedAnswerId == -1){
        return null;
    }

    return "#quizy-answer-" + SingleTestController.activeQuestion.selectedAnswerId;
};