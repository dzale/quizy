Vue.config.debug = true;
Vue.http.options.emulateJSON = true;

new Vue({
  el: '#new-test',
  data : {
      modalProperty:{class:"",display:"none"},
      questions : [],
      activeQuestion : {text : "", answers : [], points :0, numberOfAnswers : 0},
      activeQuestionIndex : 0,
      answer : {text : "", correct : false},
      correctAnswerIndex : 0,
      numberOfQuestions : 0,
      isFormSubmitted : false,
      errors:[],
      navbarStart : 0,
      navbarEnd : 1,
      navbar : [0],
      disableRight : true,
      disableLeft : true,
      activePage: 1,
      activeTest : {name : "", number_of_question : 0, picture : "", shufle: true, share_text : "", questions : []},
      leftBtn : {text: $_lang.previous, disabled:true},
      rightBtn : {text: $_lang.next, disabled:false},
      progressBar : 33,
      quickStartColorTemplate : "quizyAddWrap",
      quickStartColor : "",
      userInfo : {
          sessionInfo       : true,
          //username
          db_username_table : "",
          db_username_id    : "",
          db_username_field : "",
          //name
          db_name_table     : "",
          db_name_id        : "",
          db_name_field     : "",
          //old
          db_old_table      : "",
          db_old_id         : "",
          db_old_field      : "",
          //gender
          db_gender_table   : "",
          db_gender_id      : "",
          db_gender_field   : ""
      },
      isUpdate : false
  },
  ready:function(){
        this.quickStartColor = this.quickStartColorTemplate + this.activePage;
        var testID = this.getUrlParameter("id");
        if(testID != undefined){
            this.getTest(testID);
            this.isUpdate = true;
        }
        else{
            this.isUpdate = false;
            this.addQuestion();  
        }
  },

    methods: {
        
        nextPage: function() {
            if (this.activePage == 3) {
                //this.addTest();
                this.onFinishClick();
                return;
            }
            this.activePage = this.activePage+1;
            this.leftBtn.disabled = false;
            if (this.activePage == 3){
                this.rightBtn.text = $_lang.finish;
            }
            this.prepareStep();
        },
    
        previousPage: function() {
            if (this.activePage==1)
                return;

            this.activePage = this.activePage-1;
            
            this.rightBtn.text = $_lang.next;

            if (this.activePage == 1){
                this.leftBtn.disabled = true;
            }
            
            this.prepareStep();

        },
    
        prepareStep: function(){
            
            this.quickStartColor = this.quickStartColorTemplate + this.activePage;
            
            this.progressBar = this.activePage*33;
            if(this.progressBar == 99){
                this.progressBar = 100;
            }
/*            console.log(this.activePage);
            console.log("Left btn " + this.leftBtn.disabled);
            console.log("Right btn " + this.rightBtn.disabled);*/
            
        },

        checkLogin: function () { 
            var data = {
                action      : "login",
                username    : this.username,
                password    : this.password
            };
            this.errors = [];
            this.$http.post('/Quizy/Engine/Routes.php',
                data,
                function(response, status, request){
                    console.log(response);
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        },
        
        addQuestion: function () {
            this.activeQuestion = {text : "", answers : [], points : 0, numberOfAnswers : 2  };
            this.activeQuestion.answers.push({text : "", correct : true});
            this.activeQuestion.answers.push({text : "", correct : false});            
            this.questions.push(this.activeQuestion);
            this.activeQuestionIndex = this.questions.length - 1;
            this.numberOfQuestions =  this.numberOfQuestions + 1;
            this.navbarEnd = this.numberOfQuestions;
            if(this.numberOfQuestions >5){
                this.navbarStart = this.numberOfQuestions - 5;
            }            
            this.prepareNavbar();
            
            //console.log(this.questions);
        },
        
        deleteQuestion: function () {
            if(this.questions.length <= 1){
                return;
            }
            
            this.questions.$remove(this.activeQuestion);
            this.numberOfQuestions =  this.numberOfQuestions - 1;
            this.navbarEnd = this.numberOfQuestions;
            this.activeQuestionIndex = 0;
            this.navbarStart = 0;
            this.navbarEnd = this.numberOfQuestions > 5 ? 5 : this.numberOfQuestions;
            this.activeQuestion = this.questions[0];
//            if(this.numberOfQuestions >5){
//                this.navbarStart = this.numberOfQuestions - 5;
//            }
            this.prepareNavbar();
        },
        
        addAnswer: function () {
            this.activeQuestion.numberOfAnswers =  this.activeQuestion.numberOfAnswers + 1;
            this.activeQuestion.answers.push({text : "", correct : false});   
        },
        
        deleteAnswer: function(answer){
            var index = this.activeQuestion.answers.indexOf(answer);
            if(index > 1){
                if(answer.correct)
                    this.activeQuestion.answers[0].correct = true;
                this.activeQuestion.answers.$remove(answer);
                this.activeQuestion.numberOfAnswers =  this.activeQuestion.numberOfAnswers - 1;
                this.correctAnswerIndex = 0;
            }
            
        },
        
        onFinishClick : function(){
            if(this.isUpdate){
                this.updateTest();
            }
            else{
                this.addTest();
            }
        },
        
        addTest : function (){
            this.activeTest.questions = this.questions;
            this.activeTest.number_of_question = this.questions.length;
            this.activeTest.shufle =  jQuery('#shufle').prop('checked');
            var data = {
                action          : "addTest",
                test            : this.activeTest,
                userInfo        : this.userInfo,
                defaultUserInfo : this.userInfo.sessionInfo
            };
            this.errors = [];
            this.$http.post('Engine/Routes.php',
                data,
                function(response, status, request){
                    if(status == 200){
                        document.location.href = "./allTests.php";
                    }
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        },
        
        updateTest : function (){
            this.activeTest.questions = this.questions;
            this.activeTest.number_of_question = this.questions.length;
            this.activeTest.shufle =  jQuery('#shufle').prop('checked');
            var data = {
                action      : "updateTest",
                test        : this.activeTest
            };
            this.errors = [];
            this.$http.post('Engine/Routes.php',
                data,
                function(response, status, request){
                    if(status == 200){
                        document.location.href = "./allTests.php";
                    }
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        },
        
        getTest : function (id){
            var data = {
                action      : "getTest",
                test        : {id : id}
            };
            this.errors = [];
            this.$http.post('Engine/Routes.php',
                data,
                function(response, status, request){
                    if(status == 200){
                        this.activeTest = response.data;
                        this.activeQuestion = this.activeTest.questions[0];
                        this.questions = this.activeTest.questions;
                        this.userInfo = this.activeTest.userInfo;
                        this.userInfo.sessionInfo = true;
                        this.numberOfQuestions = this.activeTest.questions.length;
                        if(this.activeTest.shufle === "false"){
                            $('#shufle').bootstrapToggle('off');
                        }
                        if(this.numberOfQuestions > 5){
                            this.navbarEnd = 5;
                            this.disableRight = false;
                        }
                        else{
                            this.navbarEnd = this.numberOfQuestions;
                        }
                        for(var i=1;i<this.numberOfQuestions;i++){
                            this.navbar.push(i);
                        }
                        
                        for(var i=0;i<this.activeTest.questions[0].answers.length;i++){
                            if(this.activeTest.questions[0].answers[i].correct == "true"){
                                this.correctAnswerIndex = i;
                                return;
                            }
                        }
                        
                    }
                }).error(function (data, status, request) {
                    this.errors = data.message;
            });
        },
          
        onChangeShufle : function () {
            this.activeTest.shufle = !this.activeTest.shufle;
            console.log(this.activeTest.shufle);
        },
        
        onSelectedAnswerChange: function () {
            for(var i = 0; i<this.activeQuestion.answers.length;i++){
                if(i == this.correctAnswerIndex)
                    this.activeQuestion.answers[i].correct = true; 
                else
                    this.activeQuestion.answers[i].correct = false; 
            }
        },
        
        activeQuestionChanged: function (i) {
            this.activeQuestion = this.questions[i];
            for(var j=0;j<this.activeQuestion.answers.length;j++){
                if(this.activeQuestion.answers[j].correct === true || this.activeQuestion.answers[j].correct === "true"  ){
                    this.correctAnswerIndex = j;
                    console.log(j); 
                    console.log(this.activeQuestion); 
                }
            }
            this.activeQuestionIndex = i;
              
            if(this.numberOfQuestions - i > 2 && this.numberOfQuestions >5){
                if(i > 1){ 
                    this.navbarStart = i-2;
                    this.navbarEnd = i+3;
                }
                else{
                    this.navbarStart = 0;
                    this.navbarEnd = 5;
                }
                this.prepareNavbar();
            }
            if(this.numberOfQuestions - i < 3  && this.numberOfQuestions >5){
                this.navbarStart = this.numberOfQuestions -5;
                this.navbarEnd = this.numberOfQuestions;
                this.prepareNavbar();
            }
        },
        
        prepareNavbar : function (){
            this.navbar = [];
            for(var i=this.navbarStart; i<this.navbarEnd;i++){
                this.navbar.push(i);
            }
            if(this.navbarStart == 0){
                this.disableLeft = true;
            }
            else{
                this.disableLeft = false;
            }
            if(this.navbarEnd == this.questions.length){
                this.disableRight = true;
            }
            else{
                this.disableRight = false;
            }
        },
        
        shiftNavbar : function (direction){
            if( this.questions.length > 5 && (this.navbarEnd != this.questions.length || this.navbarStart != 0)){
                this.navbarStart = this.navbarStart + direction;
                this.navbarEnd = this.navbarEnd + direction;
                this.activeQuestionIndex = this.navbarEnd - 3;
                this.activeQuestion = this.questions[this.activeQuestionIndex];
                this.prepareNavbar();
            }
            
            
        },

        updateTestPhoto: function(picture){

            this.activeTest.picture = picture;

        },
        
        getUrlParameter: function(sParam){
            var sPageURL = window.location.search.substring(1);
            var sURLVariables = sPageURL.split('?');
            for (var i = 0; i < sURLVariables.length; i++) 
            {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) 
                {
                    return sParameterName[1];
                }
            }

        }


    },
    events: {
        'photo-changed': function(picture){
            this.updateTestPhoto(picture);
        }
    }
  
});

