Vue.config.debug = true;
Vue.http.options.emulateJSON = true;

new Vue({
  el: '#login',
  data : {
      modalProperty:{class:"",display:"none"},
      username : "",
      password : "",
      isFormSubmitted : false,
      errors:[]    
  },
  ready:function(){

  },

    methods: {
        checkLogin: function () { 
            var data = {
                action      : "login",
                username    : this.username,
                password    : this.password
            };
            this.errors = [];
            this.$http.post('/Quizy/Engine/Routes.php',
                data,
                function(response, status, request){
                    console.log(response);
                }).error(function (data, status, request) {
                    //console.log(data);
                    this.errors = data.message;
            });
        }

    }
      
      ,
    events: {
        'bank-changed': function () { 
            
        }
    }
  
});

