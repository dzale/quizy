<?php   
    include 'templates/header.php';
    include 'templates/top-menu.php';    
?>

    <div class="row">
        <?php 
            include 'templates/left-menu.php';
        ?>    
        <div id="page-content-wrapper" class="col-lg-8 col-lg-offset-2">

            <div id="all-tests">
                <div class="row control-group form-group" v-show="tests.length > 0">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" v-for="test in tests">
                        <div class="panel panel-default panel-media">
                            <div class="panel-heading">
                                {{test.name}}
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <img class="media-object" v-bind:src="test.picture == '' ? './Assets/img/quizy-default-test.png' : './upload/tests/' + test.picture">                                      
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-left">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <span class="text-info"><b><?php echo Lang::get('quizy_id'); ?></b></span> - {{test.id}}                                                 
                                            </div>
                                        </div>
                                        
<!--                                        <div class="row control-group form-group">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <span class="text-info"><b> <?php echo Lang::get('questions'); ?> </b></span> - {{test.number_of_question}}
                                            </div>
                                        </div>-->
                                        
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                        <span class="help-block">(<?php echo Lang::get('quizy_id_help'); ?>)</span>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                                <div class="row">
                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <a class="btn btn-primary btn-block" href="#" v-on:click="editTest(test)">
                                            <i class="fa fa-edit fa-lg"></i> <?php echo Lang::get('edit_button'); ?>
                                        </a>                                                                              
                                    </div>
                                   <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <a class="btn btn-danger btn-block" href="#" v-on:click="deleteTest(test)">
                                            <i class="fa fa-trash-o fa-lg"></i> <?php echo Lang::get('delete_button'); ?>
                                        </a>
                                    </div>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="alert alert-info text-center" v-show="tests.length == 0">
                    <?php echo Lang::get('no_quizies'); ?>
                </div>
            </div>
        </div>    
    </div>

    <script src="./Assets/js/quizy.js" type="text/javascript"></script>
    <script src="./Library/js/Controllers/allTests.js" type="text/javascript"></script>
<?php    
    include 'templates/footer.php';

