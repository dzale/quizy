// grab our gulp packages
var gulp  = require('gulp'),
    gutil = require('gulp-util'),
    concat = require('gulp-concat'),
    del = require('del');


gulp.task('delete-old-js', function() {
    return del(['./Assets/js/quizy.js']);
});

gulp.task('delete-old-css', function() {
    return del(['./Assets/css/quizy.css']);
});


gulp.task('mix-js', ['delete-old-js'], function() {
    return gulp.src([
        'vendor/js/*.js',
        'Library/js/Components/*.js',
        'Library/js/Helpers/*.js'
    ]).pipe(concat('quizy.js')).pipe(gulp.dest('./Assets/js/'));
});

gulp.task('mix-css', ['delete-old-css'], function() {
    return gulp.src([
        'vendor/css/*.css',
        'Library/css/*.css'
    ]).pipe(concat('quizy.css')).pipe(gulp.dest('./Assets/css/'));
});

gulp.task('default', ['mix-js', 'mix-css'], function() {

});

var watcherJS = gulp.watch([
    'vendor/js/*.js',
    'Library/js/Components/*.js',
    'Library/js/Helpers/*.js'
], ['mix-js']);

var watcherCSS = gulp.watch([
    'vendor/css/*.css',
    'Library/css/*.css'
], ['mix-css']);

watcherJS.on('change', function(event) {
    console.log('JS File ' + event.path + ' was ' + event.type + ', running tasks...');
});

watcherCSS.on('change', function(event) {
    console.log('CSS File ' + event.path + ' was ' + event.type + ', running tasks...');
});