<?php   
    include 'templates/header.php';
    include 'templates/top-menu.php';    
?>

    <div class="row">
        <?php 
            include 'templates/left-menu.php';
        ?>    
        <div id="page-content-wrapper" class="col-lg-8 col-lg-offset-2">

                <div id="new-test" class="panel panel-default quizyAddWrap {{quickStartColor}}"  >                   
                       <div class="panel-body">

                        <div data-toggle="wizard" class="form-wizard wizard-horizontal bwizard clearfix" >

                                <!-- START wizard steps indicator-->
                                <ol class="row bwizard-steps clearfix clickable" role="tablist" style="width: 100%">
                                    <li class="col-lg-4 col-xs-12 text-center" v-bind:class="{'active' : activePage == 1}" role="tab" aria-selected="activePage == 1 ? true : false" style="z-index: 2;" data-target="step1"><span class="label badge-inverse">1</span><a href="#step1">
                                      </a><a href="#step1"><h4><?php echo Lang::get('step_1'); ?></h4></a><a href="#step1">
                                      </a><a href="#step1"><p><?php echo Lang::get('step_1_desc'); ?></p></a><a href="#step1">
                                   </a></li>
                                   <li class="col-lg-4 col-xs-12 text-center" v-bind:class="{'active' : activePage == 2}" role="tab" aria-selected="activePage == 2 ? true : false" style="z-index: 1;" data-target="step2"><span class="label">2</span><a href="#step2">
                                      </a><a href="#step2"><h4><?php echo Lang::get('step_2'); ?></h4></a><a href="#step2">
                                      </a><a href="#step2"><p><?php echo Lang::get('step_2_desc'); ?></p></a><a href="#step2">
                                   </a></li>
                                   <li class="col-lg-4 col-xs-12 text-center" v-bind:class="{'active' : activePage == 3}" role="tab" aria-selected="activePage == 3 ? true : false" style="z-index: 0;" data-target="step3"><span class="label">3</span><a href="#step3">
                                      </a><a href="#step3"><h4><?php echo Lang::get('step_3'); ?></h4></a><a href="#step3">
                                      </a><a href="#step3"><p><?php echo Lang::get('step_3_desc'); ?></p></a><a href="#step3">
                                   </a></li>                           					   
                                </ol>
                                <!-- span is necessary-->
                                <span class="progress progress-striped m0 progress-sm">
                                   <span v-bind:style="'width:' + progressBar +'%;'" class="progress-bar progress-bar-primary"></span>
                                </span>

                                <div class="well">
                                    <div id="step1" role="tabpanel" class="quickStep bwizard-activated" aria-hidden="false" v-show="activePage == 1">  
                                        <legend>#{{activeQuestionIndex + 1}} <span class='pull-right text-danger fa fa-remove fa-lg' v-on:click="deleteQuestion"></span></legend>
                                        <div class="row control-group form-group">
                                            <!-- Question text -->
                                            <label class="control-label col-lg-2"><?php echo Lang::get('question'); ?>* </label>
                                            <div class="controls col-lg-8">
                                                <div class="input-group">                                    
                                                    <input type="text" placeholder="" v-model="activeQuestion.text" class="input-sm form-control">
                                                    <span class="input-group-addon"><i class="fa fa-question fa"></i></span>
                                                  </div>

                                            </div>
                                        </div>
                                        <div class="row control-group form-group">
                                            <!-- Question points -->
                                            <label class="control-label col-lg-2"><?php echo Lang::get('points'); ?> </label>
                                            <div class="controls col-lg-8">
                                                <input type="number" placeholder="" v-model="activeQuestion.points" class="input-sm form-control"> <br />
                                            </div>
                                        </div>

                                        <div class="row control-group form-group"  v-bind:value="answer" v-for="(index, answer)  in activeQuestion.answers" track-by="$index">
                                            <!-- Question answer -->
                                            <label class="control-label col-lg-2 col-md-2 col-sm-12 col-xs-12"><?php echo Lang::get('answer'); ?> {{index+1}}*</label>
                                            <div class="controls col-lg-8 col-md-8 col-sm-10 col-xs-10">
                                                <input type="text" placeholder="" v-model="answer.text" class="input-sm form-control">
                                            </div>
                                            <div class="controls col-lg-1 no-left-padding" v-if="index < 2">
                                                <span class="fa fa-2x fa-question-circle" data-toggle="tooltip" data-placement="top" title="Can't delete!"></span>
                                            </div>
                                            <div class="controls col-lg-1 no-left-padding" v-if="index > 1">                                
                                                <span class="text-danger fa fa-2x fa-times-circle" v-on:click="deleteAnswer(answer)" data-toggle="tooltip" data-placement="top" title="Delete"></span>                                
                                            </div>
                                        </div>

                                        <div class="row control-group form-group">
                                            <!-- Button -->
                                            <div class="controls col-lg-offset-2 col-lg-2">
                                                <button class="btn btn-success btn-block" v-on:click="addAnswer">
                                                        <?php echo Lang::get('add_answer'); ?>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="row control-group form-group" >
                                            <!-- Question answer -->
                                            <label class="control-label col-lg-2"><?php echo Lang::get('correct_answer'); ?>:</label>
                                            <div class="controls col-lg-4">
                                                <select placeholder="" v-on:change="onSelectedAnswerChange" v-model="correctAnswerIndex" class="input-sm form-control">
                                                    <option  v-bind:value="index" v-for="(index, answer)  in activeQuestion.answers" track-by="$index">{{index+1}}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row control-group form-group">
                                            <!-- Button -->
                                            <div class="controls col-lg-offset-2 col-lg-2">
                                                <button class="btn btn-success btn-block" v-on:click="addQuestion">
                                                    <?php echo Lang::get('add_question'); ?>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="row control-group form-group">
                                            <!-- Button -->
                                            <div class="controls col-lg-offset-5 col-lg-4">
                                                <div class="btn-toolbar">
                                                    <div class="btn-group">
                                                        <a aria-label="Previous" class="btn btn-default" v-bind:class="{ 'disabled': disableLeft}" aria-disabled="{{disableLeft}}" v-on:click="shiftNavbar(-1)">
                                                            <span aria-hidden="true">&laquo;</span>
                                                        </a>
                                                        <a class="btn btn-default"  v-on:click="activeQuestionChanged(i)" v-bind:class="{'active' : activeQuestionIndex == i}" v-bind:value="i" v-for="i in navbar">{{i+1}}</a>
                                                        <a aria-label="Previous" class="btn btn-default" v-bind:class="{ 'disabled': disableRight}"  aria-disabled="{{disableRight}}" v-on:click="shiftNavbar(1)">
                                                            <span aria-hidden="true">&raquo;</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div id="step2" role="tabpanel" class="quickStep bwizard-activated" aria-hidden="false" v-show="activePage == 2">  

                                        <div class="row control-group form-group">
                                            <!-- Shuffle questions text -->
                                            <label class="control-label col-lg-2"><?php echo Lang::get('populate_information'); ?>: </label>
                                            <div class="controls col-lg-1">
                                                <input type="checkbox" id="userInfo" name="userInfo" v-model="userInfo.sessionInfo" data-on="Yes" data-off="No" data-style="ios" data-onstyle=" qsButton" data-offstyle="defalt">
                                            </div>
                                            <div class="controls col-lg-1 no-left-padding" v-show="userInfo.sessionInfo">
                                                <span class="fa fa-2x fa-info-circle" data-toggle="tooltip" data-placement="top" title="<?php echo Lang::get('toltip_userinfo_form'); ?>"></span>
                                            </div>
                                            <div class="controls col-lg-1 no-left-padding" v-show="!userInfo.sessionInfo">
                                                <span class="fa fa-2x fa-info-circle" data-toggle="tooltip" data-placement="top" title="<?php echo Lang::get('toltip_userinfo_default'); ?>"></span>
                                            </div>

                                        </div>

                                        <div v-show="userInfo.sessionInfo">
                                            <div class="row control-group form-group" >
                                                <!-- Username text -->
                                                <label class="control-label col-lg-2"><?php echo Lang::get('username_info'); ?></label>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('username_table'); ?>" v-model="userInfo.db_username_table" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('user_id'); ?>"  v-model="userInfo.db_username_id" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('username_field'); ?>"  v-model="userInfo.db_username_field" class="input-sm form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row control-group form-group" >
                                                <!-- Name text -->
                                                <label class="control-label col-lg-2"><?php echo Lang::get('name_info'); ?></label>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('name_table'); ?>" v-model="userInfo.db_name_table" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('user_id'); ?>"  v-model="userInfo.db_name_id" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('name_field'); ?>"  v-model="userInfo.db_name_field" class="input-sm form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row control-group form-group" >
                                                <!-- Age text -->
                                                <label class="control-label col-lg-2"><?php echo Lang::get('age_info'); ?></label>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('age_table'); ?>" v-model="userInfo.db_old_table" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('user_id'); ?>"  v-model="userInfo.db_old_id" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('age_field'); ?>"  v-model="userInfo.db_old_field" class="input-sm form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row control-group form-group" >
                                                <!-- Age text -->
                                                <label class="control-label col-lg-2"><?php echo Lang::get('gender_info'); ?></label>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('gender_table'); ?>" v-model="userInfo.db_gender_table" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('user_id'); ?>"  v-model="userInfo.db_gender_id" class="input-sm form-control">
                                                    </div>
                                                </div>
                                                <div class="controls col-lg-2">
                                                    <div class="input-group">                                    
                                                        <input type="text" placeholder="<?php echo Lang::get('gender_field'); ?>"  v-model="userInfo.db_gender_field" class="input-sm form-control">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="step3" role="tabpanel" class="quickStep bwizard-activated" aria-hidden="false" v-show="activePage == 3">  

                                        <div class="row control-group form-group">

                                            <!-- Test photo upload -->
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                                <upload-photo-component title="Test image" path="./upload/tests/" photo-name=''></upload-photo-component>
                                            </div>

                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                                <div class="row control-group form-group">
                                                    <!-- Test name text -->
                                                    <label class="control-label col-lg-2"><?php echo Lang::get('test_name'); ?>* </label>
                                                    <div class="controls col-lg-8">
                                                        <div class="input-group">
                                                            <input type="text" placeholder="" v-model="activeTest.name" class="input-sm form-control">
                                                            <span class="input-group-addon"><i class="fa fa-question fa"></i></span>
                                                          </div>

                                                    </div>
                                                </div>


                                                <div class="row control-group form-group">
                                                    <!-- Facebook share text -->
                                                    <label class="control-label col-lg-2"><?php echo Lang::get('share_text'); ?>* </label>
                                                    <div class="controls col-lg-8">
                                                        <div class="input-group">
                                                            <input type="text" placeholder="" v-model="activeTest.share_text" class="input-sm form-control">
                                                            <span class="input-group-addon"><i class="fa fa-facebook-square fa"></i></span>
                                                        </div>

                                                    </div>
                                                </div>

                                                <div class="row control-group form-group">
                                                    <!-- Shuffle questions text -->
                                                    <label class="control-label col-lg-2"><?php echo Lang::get('shufle_questions'); ?>* </label>
                                                    <div class="controls col-lg-8">
                                                        <input type="checkbox" id="shufle" name="shufle" v-model="activeTest.shufle" data-toggle="toggle" data-on="Yes" data-off="No" data-style="ios" data-onstyle=" qsButton" data-offstyle="defalt">
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                        </div>                
                        <div class="row text-center">   
                            <p class="text-danger" v-for="error in errors">
                                {{ error }}
                            </p>
                        </div> 
                    </div>
                    <div class="panel-footer">
                            <ul class="pager bwizard-buttons">
                                 <li class="previous" v-bind:class="{ 'disabled' : leftBtn.disabled}" role="button" aria-disabled="{{leftBtn.disabled}}" v-on:click="previousPage"><a class="qsButton ">← {{leftBtn.text}}</a></li>
                                 <li class="next" role="button" v-on:click="nextPage" ><a class="qsButton">{{rightBtn.text}} →</a></li>
                             </ul>

                    </div>
                </div>
        </div>    
    </div>

<?php
    include 'templates/Components/photoUpload.php';
?>

<script src="./Assets/js/quizy.js" type="text/javascript"></script>
<script src="./Library/js/Controllers/newTest.js" type="text/javascript"></script>

<script>
    $(function() {
        $('#shufle').bootstrapToggle();
        //$('#userInfo').bootstrapToggle();    
    });
</script>
<?php
    include 'templates/footer.php';
    
