<?php

    if(is_array($quizies)){
        $quizy = $quizies[0];
    }
    else{
        $quizy = $quizies;
    }
    
    $test = new Test();
    $test->Id($quizy[Test::ATTR_ID]);
    
    $questions = $test->getQuestions(true);

    $progress = 100 / count($questions);
    $i = 0;
?>

<div class="quizy">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">

                <h2 class="text-center">
                    <?php echo $quizy[Test::ATTR_NAME]; ?>
                    <hr style="width:100px;">
                </h2>

                <?php foreach($questions as $question): ?>
                    <div class="well <?php echo ($i > 0) ? 'hidden' : ''; ?>" id="quizy-question-<?php echo $i;?>">
                        <h4 class="bottom30">#<?php echo ++$i." ".$question[Question::ATTR_TEXT];?></h4>

                        <div class="row">
                            <?php foreach($question["answers"] as $answer): ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 bottom10">
                                    <button class="btn btn-white btn-block quizy-answer" data-quizy-answer-id="<?php echo $answer[Answers::ATTR_ID];?>" id="quizy-answer-<?php echo $answer[Answers::ATTR_ID];?>">
                                        <?php echo $answer[Answers::ATTR_TEXT]; ?>
                                    </button>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endforeach;?>
<!--                <ol class="timeline">
                    
                    <?php /*foreach($questions as $question): */?>
                        <li class="timeline-step done">
                            <input class="timeline-step-radio" id="trigger1" name="trigger" type="radio">

                            <label class="timeline-step-label" for="trigger1">
                                <span class="timeline-step-content">
                                    12 May 2013</span>
                            </label>

                            <span class="timeline-step-title">
                                Step 1</span>

                            <i class="timeline-step-marker"><?php /*echo ++$i; */?></i>
                        </li>
                    <?php /*endforeach;*/?>
                </ol>-->

                <div class="row">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info progress-striped" style="width: <?php echo $progress;?>%;"></div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">

                <div class="row">

                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 pull-left text-left">
                        <button class="btn btn-white btn-no-border btn-block" id="quizy-prev-question">
                            <i class="fa fa-long-arrow-left">  </i>
                            Back
                        </button>
                    </div>



                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 pull-right text-right bottom10">
                        <button class="btn btn-white btn-no-border btn-block" id="quizy-next-question">
                            Next
                            <i class="fa fa-long-arrow-right">  </i>
                        </button>
                    </div>

                    <div class="col-lg-6 col-md-4 col-sm-4 col-xs-12 text-center">
                        <button  href="#" class="btn btn-white">
                            1/<?php echo count($questions); ?>
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    var numOfQuestions = <?php echo count($questions); ?>;

    if (!window.jQuery) {
        document.write('<script type="text/javascript" src="<?php echo WEBSITE_DOMAIN;?>/Assets/js/jquery-1.12.0.min.js" charset="utf-8"></'+'script>');
    }
</script>


<script src="<?php echo WEBSITE_DOMAIN;?>/Library/js/Controllers/singleTestTemplate.js" type="text/javascript"></script>