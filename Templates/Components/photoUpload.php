<template id="upload-photo-component">
    <div class="panel panel-default">
        <div class="panel-heading text-center"><i class="fa fa-image"> </i> {{ title }}</div>
        <div class="panel-body">
            <div class="row form-group control-group">
                <div class="col-lg-12">
                    <img class="media-object" id="test-img" v-bind:src="photoName == '' ? './Assets/img/quizy-default-test.png' : path + photoName" width="100%">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a class="btn btn-success btn-block file-btn">
                        <i class="fa fa-camera"></i>
                        <span><?php echo Lang::get('change_photo'); ?></span>
                        <input type="file" id="photo-upload" value="Choose a file" v-on:change="onPhotoChanged">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <modal v-bind:show.sync="newPhotoSelected" v-bind:callback="onPhotoCropped">
            <div slot="modal-header" class="modal-header">
                <h4 class="modal-title"><?php echo Lang::get('crop_photo'); ?></h4>
            </div>
            <div slot="modal-body" class="modal-body">
                <div class="row control-group form-group">
                    <div id="croppie-container"></div>
                </div>
            </div>
    </modal>
</template>
