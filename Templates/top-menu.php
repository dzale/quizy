<div class="row">
    <div class="col-lg-12">
        <nav class="navbar">
          <div class="container-fluid">

            <div class="navbar-header">
                <div class="row">
                    <div class="col-lg-3">
                        <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                            <span class="hamb-top"></span>
                            <span class="hamb-middle"></span>
                            <span class="hamb-bottom"></span>
                        </button>
                    </div>
<!--                    <div class="col-lg-9">
                        <a class="navbar-brand" href="#"><?php echo Lang::get('welcome_text'); ?></a>
                    </div>-->
                </div>
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
              <ul class="nav navbar-nav pull-right">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                      Perica Peric<span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Profile</a></li>            
                    <li class="divider"></li>
                    <li><a href="/auth/logout">Logout</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </nav>
    </div>
</div>