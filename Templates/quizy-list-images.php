<div class="quizy">    
    <div class="container-fluid">
        <div class="row no-padding">
            <?php foreach($quizies as $quizy): ?>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <a href="#" class="portfolio-box">
                        <img src="<?php echo WEBSITE_DOMAIN; ?>/upload/tests/<?php echo $quizy[Test::ATTR_PICTURE] ?>" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    <?php echo $quizy[Test::ATTR_NAME] ?>
                                </div>
                                <div class="project-name">

                                </div>
                            </div>
                        </div>
                    </a>

                </div>    
            <?php endforeach; ?>
        </div>
    </div>    
</div>