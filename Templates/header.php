<?php
    include  dirname(__FILE__) . '/../Engine/Quizy.php';  
?>

<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" charset="utf-8">
            var $_lang = <?php echo Lang::all(); ?>;
        </script>
        
        <title><?php echo Lang::get('script_name'); ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="<?php echo WEBSITE_DOMAIN; ?>/Assets/css/quizy.css" rel="stylesheet" type="text/css">
       
        
    </head>
    <body>       
        <div id="wrapper" class="main-content">
            <div class="overlay"></div>

