<?php 

return array(
    
    "script_name" => "Quizy",
    
    "welcome_text" => "Welcome to Quizy",
    
    "login" => "Login",
    
    "username" => "Username",
    
    "password" => "Password",
    
    "add_question" => "Add question",
    
    "question" => "Question",
    
    "questions" => "Questions",
    
    "points" => "Points",
    
    "answer" => "Answer",
    
    "correct_answer" => "Correct answer",
    
    "add_answer" => "Add answer",
    
    "add_test" => "Add test",
    
    "previous" => "Previous",
    
    "next"  => "Next",
    
    "finish" => "Finish",
    
    "test_name" => "Test name",
    
    "picture" => "Picture",
    
    "share_text" => "Share text",
    
    "shufle_questions" => "Shuffle questions",
    
    "dashboard" => "Dashboard",
    
    "new_quizy" => "New Quizy",
    
    "your_quizies" => "Your Quizies",
    
    "change_photo" => "Change photo",
    
    "crop_photo" => "Crop selected photo",
    
    "step_1" => "Step One",
    
    "step_2" => "Step Two",
    
    "step_3" => "Step Three",
    
    "step_1_desc" => "Let's add some questions!",
    
    "step_2_desc" => "User settings",
    
    "step_3_desc" => "General Settings",
    
    "no_quizies" => "You haven't added any Quizies yet, click <a href='newTest.php'> here </a> to add some.",
    
    "edit_button" => "Edit",
    
    "delete_button" => "Delete",
    
    "quizy_id" => "Quizy Id",
    
    "quizy_id_help" => "Id is used to include Quizies on custom pages",
    
    "populate_information" => "Populate information",
    
    "toltip_userinfo_form" => "User has to populate self information after resolvilg test!",
    
    "toltip_userinfo_default" => "User information will be get from session!",
    
    "username_info" => "Username info",
    
    "username_table" => "Username table",
    
    "user_id" => "User id",
    
    "username_field" => "Username field",
    
    "name_info" => "Name info",
    
    "name_table" => "Name table",
    
    "name_field" => "Name field",
    
    "age_info" => "Age info",
    
    "age_table" => "Age table",
    
    "age_field" => "Age field",
    
    "gender_info" => "Gender info",
    
    "gender_table" => "Gender table",
    
    "gender_field" => "Gender field",
    
);
